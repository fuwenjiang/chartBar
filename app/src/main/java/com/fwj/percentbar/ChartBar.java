package com.fwj.percentbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.fwj.percentbar.bean.DataBean;

import java.util.ArrayList;

/**
 * 包名：com.fwj.percentbar
 * <p/>
 * 描述：条形图尝试
 * <p/>
 * 作者：傅文江
 * <p/>
 * 时间：2016/8/24 11:09
 * <p/>
 * 版权：Copyright © 2016-2026  北京乐客灵境科技有限公司
 */
public class ChartBar extends View {
    private static final String TAG = "ChartBar_TAG";
    /**
     * 用来画圆柱的画笔
     */
    private Paint mBarPaint;
    /**
     * 用来画圆柱的画笔
     */
    private Paint mTextPaint;
    /**
     * 用来画线条的画笔
     */
    private final Paint mLinePaint;

    /**
     * 需要画线的等级
     */
    private ArrayList<Float> levels;

    //图形原点坐标
    private int x0;
    //图形原点坐标
    private ArrayList<DataBean> datas;


    private int y0;
    //每个条形柱的宽度
    private int mBarWidth;
    private int textHeight = 20;
    private int textWidth = 33;

    //每个条形柱之间的间隔
    private int mBarSpace;

    private int mAutoBarSpace;
    //条形柱的总数
    private int mBarTotalCount;
    //条形柱的最大值
    private int max;
    private int spaceY;
    private int measuredHeight;
    private int measuredWidth;

    public ChartBar(Context context) {
        this(context, null);
    }

    public ChartBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChartBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        /* 初始化柱状图画笔*/
        //ANTI_ALIAS_FLAG抗锯齿
        mBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBarPaint.setColor(0xff000000);
        //设置画笔为实心画笔
        mBarPaint.setStyle(Paint.Style.FILL);

        /* 初始化线的画笔*/
        mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setStyle(Paint.Style.FILL);
        mLinePaint.setColor(0xff000000);
        //设置线宽
        mLinePaint.setStrokeWidth(2);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setColor(0xffaa0000);
        //设置线宽
        mTextPaint.setTextSize(16);
        mTextPaint.setStrokeWidth(1);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        //如果宽高都为wrap_content，高度为输入的max值，宽度为坐标原点距离左边的距离+（每个条形柱+间隔）*条形柱的数目
        if (widthSpecMode == MeasureSpec.AT_MOST && heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(x0 + (mBarWidth + mBarSpace) * mBarTotalCount, max + getPaddingTop() + getPaddingBottom() + textHeight);
            //如果高度为wrap_content,宽度为match_parent或者精确数值时,高度设置为max
        } else if (heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(widthSpecSize, max + getPaddingTop() + getPaddingBottom() + textHeight);
            //如果宽度为wrap_content,高度为match_parent或者精确数值时，宽度为坐标原点距离左边的距离+（每个条形柱+间隔）*条形柱的数目
        } else if (widthSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(x0 + (mBarWidth + mBarSpace) * mBarTotalCount, heightSpecSize);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        measuredHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        measuredWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        x0 = textWidth + getPaddingLeft();
        y0 = measuredHeight - textHeight + getPaddingTop();
        mBarTotalCount = datas.size();
        spaceY = (y0 - getPaddingTop()) / levels.size();
        mAutoBarSpace = (measuredWidth - x0 - mBarTotalCount * mBarWidth) / (mBarTotalCount + 1);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(x0, y0, x0, getPaddingTop()-mLinePaint.getStrokeWidth(), mLinePaint);
        //  canvas.drawRect(x0-35,getPaddingTop(),getMeasuredWidth()-getPaddingRight(),y0+textHeight,mLinePaint);
        // canvas.drawLine(x0, y0, measuredWidth, y0, mLinePaint);
        for (int i = 0; i < datas.size(); i++) {
            canvas.drawRect(x0 + (1 + i) * mAutoBarSpace + i * mBarWidth, y0 - datas.get(i).getValue(), x0 + (1 + i) * (mAutoBarSpace + mBarWidth), y0, mBarPaint);
            canvas.drawText(datas.get(i).getKey(), x0 + mAutoBarSpace + i * (mAutoBarSpace + mBarWidth), y0 + textHeight, mTextPaint);
        }
        for (int i = 0; i <= levels.size(); i++) {
            canvas.drawLine(x0, y0 - i * spaceY, getMeasuredWidth() - getPaddingRight(), y0 - i * spaceY, mLinePaint);
            canvas.drawText(max / levels.size() * i + "", x0 - textWidth, y0 - i * spaceY + 4, mTextPaint);
        }

    }

    public void setmBarWidth(int mBarWidth) {
        this.mBarWidth = mBarWidth;
    }

    public void setmBarSpace(int mBarSpace) {
        this.mBarSpace = mBarSpace;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setDatas(ArrayList<DataBean> datas) {
        this.datas = datas;
    }

    public void setLevels(ArrayList<Float> levels) {
        this.levels = levels;
    }

}
