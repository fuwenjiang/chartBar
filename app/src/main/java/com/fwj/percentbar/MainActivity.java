package com.fwj.percentbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
public class MainActivity extends AppCompatActivity {
    private PercentageBar mBarGraph;
    private ArrayList<Float> respectTarget;
    private ArrayList<String> respName;
    private PercentageHBar mBarGraph2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        respectTarget = new ArrayList<Float>();
        respName = new ArrayList<String>();
        respectTarget.add(35.0f);
        respectTarget.add(20.0f);
        respectTarget.add(18.0f);
        respectTarget.add(10.0f);
        respName.add("滴滴");
        respName.add("小米");
        respName.add("京东");
        respName.add("美团");
        mBarGraph = (PercentageBar) findViewById(R.id.bargraph);
        mBarGraph.setRespectTargetNum(respectTarget);
        mBarGraph.setRespectName(respName);
        mBarGraph.setTotalBarNum(respectTarget.size());
        mBarGraph.setMax(40);
        mBarGraph.setBarWidth(40);
        mBarGraph.setVerticalLineNum(4);
        mBarGraph.setUnit("亿");
        mBarGraph2 = (PercentageHBar) findViewById(R.id.bargraphh);
        mBarGraph2.setRespectTargetNum(respectTarget);
        mBarGraph2.setRespectName(respName);
        mBarGraph2.setTotalBarNum(respectTarget.size());
        mBarGraph2.setMax(300);
        mBarGraph2.setBarWidth(40);
        mBarGraph2.setHorizontalLineNum(3);
        mBarGraph2.setUnit("亿");
    }
}
