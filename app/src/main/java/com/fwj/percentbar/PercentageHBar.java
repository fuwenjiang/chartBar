package com.fwj.percentbar;

/**
 * 包名：${PACKAGE_NAME}
 * <p/>
 * 描述：
 * <p/>
 * 作者：傅文江
 * <p/>
 * 时间：15/10/30 10:06
 * <p/>
 * 版权：Copyright © 2016-2026  北京乐客灵境科技有限公司
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class PercentageHBar extends View {
    private static final String TAG = "PercentageBarTAG";
    //画线的画笔
    private Paint mLinePaint;
    //画柱状图的画笔
    private Paint mBarPaint;

    //写字的画笔
    private Paint mTextPaint;
    //开始X坐标
    private int startX;
    //开始Y坐标
    private int startY;

    //结束X坐标
    private int stopY;
    //测量值 宽度
    private int measuredWidth;
    //测量值 高度
    private int measuredHeight;
    //每条柱状图的宽度
    private int barWidth;
    //设置最大值，用于计算比例
    private float max;
    //设置每条柱状图的目标值，除以max即为比例
    private ArrayList<Float> respTarget;
    //设置一共有几条柱状图
    private int totalBarNum;
    //设置每条柱状图的当前比例
    private Float[] currentBarProgress;
    //每条竖线的当前比例
    private int currentPerLineProgress;
    //最上面一条横线的比例
    private int currentHorizentalLineProgress;
    //每条柱状图的名字
    private ArrayList<String> respName;
    //每条竖线之间的间距
    private int deltaX;
    //每条柱状图之间的间距
    private int deltaY;
    //一共有几条竖线
    private int horizontalLineNum;
    //单位
    private String unit;

    //每条竖线之间相差的值
    private float numPerUnit;

    private int x0 = 0;
    private int y0 = 0;

    public PercentageHBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public PercentageHBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PercentageHBar(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        //设开始X坐标为0
        startX = 0;
        //设开始Y坐标为50
        startY = 50;

        /* 初始化柱状图画笔*/
        //ANTI_ALIAS_FLAG抗锯齿
        mBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBarPaint.setColor(0xff40E0D0);
        //设置画笔为实心画笔
        mBarPaint.setStyle(Style.FILL);


       /* 初始化线的画笔*/
        mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setStyle(Style.FILL);
        mLinePaint.setColor(0xffcdcdcd);
        //设置线宽
        mLinePaint.setStrokeWidth(2);


    }

    /**
     * 测量方法，主要考虑宽和高设置为wrap_content的时候，我们的view的宽高设置为多少
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);

        //如果宽高都为wrap_content的时候，我们将高设置为我们输入的max值，也就是柱状图的最大值
        //宽度为每条柱状图的宽度加上间距再乘以柱状图条数再加上开始X值后得到的值
        if (widthSpecMode == MeasureSpec.AT_MOST && heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension((barWidth + 2 * 10) * totalBarNum + startX, (int) max);
        }
        //如果高度为Wrap_content,宽度为match_parent或者精确数值的时候
        else if (heightSpecMode == MeasureSpec.AT_MOST) {
            //高度设置为max，宽度为match_parent或者精确数值
            setMeasuredDimension(widthSpecSize, (int) max);
        }
        //如果为宽度为wrap_content,高度为match_parent或者精确数值的时候
        else if (widthSpecMode == MeasureSpec.AT_MOST) {
            //宽度为每条柱状图的宽度加上间距再乘以柱状图条数再加上开始X值后得到的值,高度设置为父容器的宽度，
            setMeasuredDimension((barWidth + 2 * 10) * totalBarNum + startX, heightSpecSize);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        measuredWidth = getMeasuredWidth();
        measuredHeight = getMeasuredHeight();
        startY = measuredHeight;
        stopY = barWidth;
        //计算出每条竖线所代表的数值
        numPerUnit = max / horizontalLineNum;
        deltaX = (getMeasuredWidth() - startX - barWidth * totalBarNum) / totalBarNum;

        currentHorizentalLineProgress = getMeasuredHeight();
        x0 = startX + deltaX + (int) (barWidth * 0.7);
        y0 = getMeasuredHeight() - (int) (barWidth * 1.2);
        deltaY = (getMeasuredHeight() - y0) / horizontalLineNum;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /**
         * 画柱状图
         */
        for (int i = 0; i < totalBarNum; i++) {
            if (currentBarProgress[i] < (respTarget.get(i) / max) * stopY) {
                currentBarProgress[i] += 10;
                postInvalidateDelayed(10);
            }

            canvas.drawText(respName.get(i), x0 + 10 + i * (deltaX + (int) (0.45 * barWidth)), startY - 9, mTextPaint);
            // canvas.drawRect(startX + 7 * barWidth / 5, startY + deltaY + i * (deltaY + barWidth), currentBarProgress[i], startY + deltaY + i * (deltaY + barWidth) + barWidth, mBarPaint);
            canvas.drawRect(x0 + 10 + i * (deltaX + (int) (0.45 * barWidth)),
                    getMeasuredHeight() - i * (deltaY + barWidth) - 50,
                    x0 + 10 + i * (deltaX + (int) (0.45 * barWidth)) + barWidth,
                    getMeasuredHeight() - 50, mBarPaint);
        }
        /**
         * 画横线
         */
        for (int i = 0; i < horizontalLineNum; i++) {
            if (currentPerLineProgress < measuredHeight) {
                currentPerLineProgress += 3;
                postInvalidateDelayed(10);
            }
//            canvas.drawLine((startX + 7 * barWidth / 5) + (i + 1) * deltaX, startY, (startX + 7 * barWidth / 5) + (i + 1) * deltaX, currentPerLineProgress, mLinePaint);
            //canvas.drawText(numPerUnit * (i + 1) + unit, (startX + 7 * barWidth / 5) + (i + 1) * deltaX - barWidth, startY - barWidth / 5, mTextPaint);
            canvas.drawText(numPerUnit * (i + 1) + unit, startX + barWidth / 5, (startY + 7 * barWidth / 5) - (i + 1) * deltaX - barWidth, mTextPaint);
            canvas.drawLine(x0, (startY + 7 * barWidth / 5) - (i + 1) * deltaX - barWidth, x0 + currentPerLineProgress, (startY + 7 * barWidth / 5) - (i + 1) * deltaX - barWidth, mLinePaint);
        }
        /**
         * 画最左面的横线
         */
        if (currentHorizentalLineProgress > startY - getMeasuredHeight()) {
            currentHorizentalLineProgress -= 10;
            postInvalidateDelayed(10);
        }
        canvas.drawLine(startX + deltaX + (int) (barWidth * 0.7) + startX,
                startY - (int) (barWidth * 1.59),
                startX + deltaX + (int) (barWidth * 0.7) + startX,
                currentHorizentalLineProgress - (int) (barWidth * 1.59),
                mLinePaint);
    }

    /**
     * 设置每个柱状图的宽度
     *
     * @param width
     */
    public void setBarWidth(int width) {
        this.barWidth = width;
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextSize(3 * barWidth / 5);
        mTextPaint.setStrokeWidth(1);
        mTextPaint.setColor(0xffababab);


    }

    /**
     * 设置最大值
     *
     * @param max
     */
    public void setMax(float max) {
        this.max = max;
    }

    /**
     * 设置一共有几个柱状图
     *
     * @param totalNum
     */
    public void setTotalBarNum(int totalNum) {
        this.totalBarNum = totalNum;
        currentBarProgress = new Float[totalNum];
        for (int i = 0; i < totalNum; i++) {
            currentBarProgress[i] = 0.0f;
        }
    }

    /**
     * 分别设置每个柱状图的目标值
     *
     * @param respTarget
     */
    public void setRespectTargetNum(ArrayList<Float> respTarget) {
        this.respTarget = respTarget;

    }

    /**
     * 分别设置每个柱状图的名字
     *
     * @param respName
     */
    public void setRespectName(ArrayList<String> respName) {
        this.respName = respName;
    }

    /**
     * 设置单位
     *
     * @param unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * 设置有几条竖线
     *
     * @param num
     */
    public void setHorizontalLineNum(int num) {
        this.horizontalLineNum = num;
    }


}
