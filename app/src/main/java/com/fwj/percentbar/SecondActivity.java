package com.fwj.percentbar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fwj.percentbar.bean.DataBean;

import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {

    private ChartBar charBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        charBar = (ChartBar) findViewById(R.id.charBar);
        charBar.setMax(500);
        charBar.setmBarSpace(50);
        charBar.setmBarWidth(30);
        ArrayList<DataBean> datas = new ArrayList<>();
        datas.add(new DataBean("小米", 490));
        datas.add(new DataBean("360", 410));
        datas.add(new DataBean("滴滴", 120));
        datas.add(new DataBean("百度", 210));
        charBar.setDatas(datas);
        ArrayList<Float> levels=new ArrayList();
        levels.add(100f);
        levels.add(200f);
        levels.add(300f);
        levels.add(400f);
        levels.add(500f);
        charBar.setLevels(levels);
    }
}
