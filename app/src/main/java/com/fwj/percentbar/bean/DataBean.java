package com.fwj.percentbar.bean;

/**
 * 包名：com.fwj.percentbar.bean
 * <p/>
 * 描述：
 * <p/>
 * 作者：傅文江
 * <p/>
 * 时间：2016/8/24 14:37
 * <p/>
 * 版权：Copyright © 2016-2026  北京乐客灵境科技有限公司
 */
public class DataBean {
    private String key;
    private int value;

    public DataBean(String key, int value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public int getValue() {
        return value;
    }
}
